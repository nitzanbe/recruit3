@extends('layouts.app')
@section('content')
@section('title', 'users')
    <h1>Users</h1>
    <table class = "table table-dark">
        <tr> 
            <th>Id</th><th>Name</th><th>Email</th><th>Department</th><th>Roles</th><th>Created</th><th>Updated</th>
        </tr>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>
                @foreach($userroles as $userrole)
                    @if(($userrole->user_id)==($user->id))
                        {{$userrole->role->name}}
                    @endif
                @endforeach
            </td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a> 
            </td> 
            <td>
                <a href = "{{route('user.show',$user->id)}}">Details</a>
            </td>
            <td>
                @foreach($userroles as $userrole)
                    @if($userrole->user_id==$user->id && $userrole->role->name =='admin')
                        @foreach($userroles as $userrole)
                            @if($userrole->user_id==$user->id && $userrole->role->name !='manager')
                                <a href = "#">make manager</a>
                            @endif
                            @if($userrole->user_id==$user->id && $userrole->role->name =='manager')
                                <a href = "#">delete manager</a>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </td>    
        </tr>
        @endforeach
    </table>
@endsection
