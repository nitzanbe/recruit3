@extends('layouts.app')

@section('title', 'User')

@section('content')
    <h1>User details</h1>
    <table class = "table table-dark">
        <tr>
            <td>Id</td>
            <td>{{$user->id}}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$user->email}}</td>
        </tr> 
        <tr>   
        <tr>
            <td>Department</td> 
            <td>
                @if(Gate::allows('change-dep'))
                    <form method="POST" action="{{ route('users.changeDepartmentFromUser') }}">          
                        @csrf
                        <div class="col-md-6">
                            @if (isset($user->department_id) ) 
                                {{$user->department->name}}
                            @else 
                                No department assigned yet
                            @endif
                            <select class="form-control" name="department_id">
                                @foreach ($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach 
                            </select>
                            <input name="id" type="hidden" value = {{$user->id}} >
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Change department
                                    </button>
                                </div>
                            </div>  
                        </div>
                    </form>
                @else
                    @if (isset($user->department_id) ) 
                        {{$user->department->name}}
                    @else 
                        No department assigned yet
                    @endif
                @endif
            </td> 
        </tr> 
        <tr>
            <td>Created</td>
            <td>{{$user->created_at}}</td>
        </tr>
        <tr>
            <td>Updated</td>
            <td>{{$user->updated_at}}</td>  
        </tr>    
    </table>
@endsection
