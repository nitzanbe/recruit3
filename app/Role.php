<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
   public function users(){
      return $this->belongsToMany('App\User','userroles');        
   }
   public function userrole(){
      return $this->hasMany('App\Userrole');
   }
}



